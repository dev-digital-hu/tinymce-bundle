## Build

```bash
npm install
npm run build
```

As the result, bundled tinymce editor will be available under `./dist/index.js`. 

## Publish

> NPM_TOKEN=<GITLAB_PERSONAL_ACCESS_TOKEN> npm publish

## Usage

### Javascript 

Include `<script src="index.js"></script>` and use the following global members.

- `applySkin('dark');` - apply `dark` or `light` skin
- `contentStyle('dark')` - get the `dark` or `light` css content style to be used upon initializing new editor
- `tinymce` - refer the tinymce editor with all open-source plugins and `silver` theme

### Node

```typescript
const { tinymce, applySkin, contentStyle } = require('@dev-digital-hu/tinymce-bundle');
```

### Example

```html
<script src="index.js"></script>
<script>
    function heading() {
        const element = document.createElement('h1');
        element.innerText = 'TinyMCE Webpack demo';
        return element;
    }
    
    const parent = document.createElement('p');
    
    function editorArea() {
        const element = document.createElement('textarea');
        element.id = 'editor';
        return element;
    }
    
    parent.appendChild(editorArea());
    
    document.body.appendChild(heading());
    document.body.appendChild(parent);
    
    tinymce.init({
        selector: 'textarea#editor',
        plugins: 'advlist code emoticons link lists table',
        toolbar: 'bold italic | bullist numlist | link emoticons',
        skin: false,
        content_css: false,
        language: 'hu_HU',
        content_style: contentStyle('dark')
    });
    
    applySkin('dark');
</script>
```

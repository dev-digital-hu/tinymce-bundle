/* Import TinyMCE */
import editor from 'tinymce/tinymce.min';

import 'tinymce/models/dom/model.min';

/* Additional languages */
import 'tinymce-i18n/langs6/hu_HU';

/* Default icons are required for TinyMCE 5.3 or above */
import 'tinymce/icons/default/icons.min';

/* A theme is also required */
import 'tinymce/themes/silver/theme.min';

/* Import the skin */
import skinStyle from 'tinymce/skins/ui/tinymce-5/skin.min.css';
import skinStyleDark from 'tinymce/skins/ui/tinymce-5-dark/skin.min.css';

/* Import plugins */
import 'tinymce/plugins/advlist/plugin.min';
import 'tinymce/plugins/autolink/plugin.min';
import 'tinymce/plugins/lists/plugin.min';
import 'tinymce/plugins/link/plugin.min';
import 'tinymce/plugins/image/plugin.min';
import 'tinymce/plugins/charmap/plugin.min';
import 'tinymce/plugins/preview/plugin.min';
import 'tinymce/plugins/anchor/plugin.min';
import 'tinymce/plugins/searchreplace/plugin.min';
import 'tinymce/plugins/visualblocks/plugin.min';
import 'tinymce/plugins/code/plugin.min';
import 'tinymce/plugins/fullscreen/plugin.min';
import 'tinymce/plugins/insertdatetime/plugin.min';
import 'tinymce/plugins/media/plugin.min';
import 'tinymce/plugins/table/plugin.min';
import 'tinymce/plugins/code/plugin.min';
import 'tinymce/plugins/wordcount/plugin.min';
import 'tinymce/plugins/emoticons/plugin.min';
import 'tinymce/plugins/emoticons/js/emojis.min';

import contentUiCss from 'tinymce/skins/ui/tinymce-5/content.min.css';
import contentCss from 'tinymce/skins/content/tinymce-5/content.min.css';

import contentUiCssDark from 'tinymce/skins/ui/tinymce-5-dark/content.min.css';
import contentCssDark from 'tinymce/skins/content/tinymce-5-dark/content.min.css';

const contentStyle = (variant) => {
  if (variant === 'dark') {
    return contentUiCssDark.toString() + '\n' + contentCssDark.toString();
  } else {
    return contentUiCss.toString() + '\n' + contentCss.toString();
  }
}

const applySkin = (variant) => {
  const getStyleElement = () => {
    const id = 'tinymceBundleSkin';
    if (document.getElementById(id)) {
      return document.getElementById(id);
    }
    const style = document.createElement('style');
    style.setAttribute('id', id);
    document.body.appendChild(style);
    return style;
  }

  getStyleElement().innerHTML = (variant === 'dark' ? skinStyleDark : skinStyle).toString();
}

const tinymce = editor;

export {
  tinymce, applySkin, contentStyle
}

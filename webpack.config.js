const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/index.js',
  resolve: {
    extensions: ['.js']
  },
  module: {
    rules: [
      {
        test: /skin\.min\.css$/i,
        use: ['css-loader'],
      },
      {
        test: /content\.min\.css$/i,
        use: ['css-loader'],
      },
    ],
  },
  optimization: {
    minimize: true,
  },
  performance: {
    maxAssetSize: 2000000,
    maxEntrypointSize: 2000000
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
    libraryTarget: 'commonjs',
    clean: true,
  },
};
